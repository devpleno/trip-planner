import React, { Component } from 'react'
import { View, Text, Image, TextInput, TouchableOpacity, Dimensions, AsyncStorage } from 'react-native'

import MapView, { Marker } from 'react-native-maps'

import assets from './assets'
import styles from './styles'

class AddTripScreen extends Component {

    static navigationOptions = {
        header: null
    }

    state = {
        trip: '',
    }

    handleSave = async () => {
        const newTrip = {
            id: new Date().getTime(),
            trip: this.state.trip,
            price: 0,
            latidude: 0,
            longitude: 0
        }

        const trips = await AsyncStorage.getItem("trips")
        let colTrips = []

        if (trips) {
            colTrips = JSON.parse(trips)
        }

        colTrips.push(newTrip)
        await AsyncStorage.setItem("trips", JSON.stringify(colTrips))

        this.props.navigation.state.params.refresh();
        this.props.navigation.goBack();
    }

    render() {

        return (
            <View style={{ flex: 1 }}>
                <View style={styles.imagem}>

                    <TextInput style={styles.campo} placeholder="Nome da viagem" onChangeText={(ev) => this.setState({ trip: ev })}></TextInput>

                    <TouchableOpacity style={styles.btnSalvar} onPress={this.handleSave}>
                        <Text>Salvar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default AddTripScreen