import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    imagem: {
        height: 144,
        backgroundColor: 'pink',
        marginBottom: 6
    },
    titulo: {
        backgroundColor: 'green',
    },
    valor: {
        backgroundColor: 'cyan',
        padding: 4,
        position: 'absolute',
        right: 32,
        top: 100,
        textAlign: 'right'
    }
}) 