import React from 'react'
import { View, Text, Dimensions, TouchableOpacity } from 'react-native'

import styles from './styles'

const Trip = (props) => {
    const dim = Dimensions.get('window')

    return (
        <TouchableOpacity onPress={props.onPress}>
            <View style={[styles.imagem, { width: dim.width - 32 }]}><Text>imagem</Text></View>
            <Text style={styles.titulo}>{props.title}</Text>
            <Text style={styles.valor}>{props.price}</Text>
        </TouchableOpacity>
    )
}

export default Trip