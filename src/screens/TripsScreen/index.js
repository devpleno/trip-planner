import React, { Component } from 'react'
import { View, Text, FlatList, TouchableOpacity, Image, AsyncStorage } from 'react-native'

import MapView from 'react-native-maps'
import styles from './styles'
import Trip from './Trip'
import isIphoneX from '../../utils/isIphoneX'

class TripsScreen extends Component {

    state = {
        trips: []
    }

    renderItem = (obj) => {
        return <Trip onPress={() => this.props.navigation.navigate('Trip', { id: obj.item.id, refresh: this.loadData })} title={obj.item.trip} price={obj.item.price} />
    }

    componentDidMount() {
        this.loadData()
    }

    loadData = async () => {
        const tripsAS = await AsyncStorage.getItem('trips')

        this.setState({
            trips: JSON.parse(tripsAS)
        })
    }

    handleItemChange = (info) => {
        const {viewableItems} = info

        if(viewableItems && viewableItems.lenght > 0) {
            const [item] = viewableItems

            this.map.animateToRegion(
                this.regionFrom(item.item.latitude, item.item.longitude, 1000),
                2500
            )
        }
    }

    regionFrom = (lat, lon, distance) => {
        distance = distance / 2
        const circumference = 40075
        const oneDegreeOfLatitudeInMeters = 111.32 * 1000
        const angularDistance = distance / circumference
        
        const latitudeDelta = distance / oneDegreeOfLatitudeInMeters
        const longitudeDelta = Math.abs(
            Math.atan2(
                Math.sin(angularDistance)*Math.cos(lat),
                Math.cos(angularDistance)- Math.sin(lat)*Math.sin(lat)
            )
        )

        return result = {
            latitude: lat,
            longitude: lon,
            latitudeDelta,
            longitudeDelta
        } 
    }

    render() {

        const { trips } = this.state

        return (
            <View style={styles.background}>
                <View style={styles.map}>
                    <MapView
                        style={{ flex: 1 }}
                        initialRegion={{
                            latitude: 37.78825,
                            longitude: -122.4324,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421
                        }}
                        ref={ref => this.map = ref}
                    />

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('AddTrip', { refresh: this.loadData })} style={styles.btnAddTrip}>
                        <Image source={require('../../../assets/images/Union.png')} />
                    </TouchableOpacity>
                </View>

                <View style={[styles.list]}>
                    <FlatList onViewableItemsChanged={this.handleItemChange}
                    data={trips} renderItem={this.renderItem} keyExtractor={i => i.id.toString()} horizontal pagingEnabled style={[isIphoneX() ? { marginBottom: 20 } : null]} />
                </View>
            </View>
        )
    }
}

export default TripsScreen