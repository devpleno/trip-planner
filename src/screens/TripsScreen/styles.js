import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    background: {
        flex: 1,
        backgroundColor: 'blue',
        justifyContent: 'space-around',
        alignItems: 'stretch'
    },
    map: {
        flex: 1,
        backgroundColor: 'grey',
    },
    list: {
        backgroundColor: 'white',
        padding: 16
    },
    btnAddTrip: {
        position: 'absolute',
        top: 20,
        right: 20,
        padding: 10
    }
}) 