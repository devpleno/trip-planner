export default assets = {
    background: require('../../../assets/images/Group.png'),
    tripplanner: require('../../../assets/images/Tripplanner.png'),
    devpleno: require('../../../assets/images/devpleno.png'),
    arrowRight: require('../../../assets/images/arrow-right.png'),
    pin: require('../../../assets/images/pin.png')
}