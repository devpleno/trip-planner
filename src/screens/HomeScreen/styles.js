import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    background: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        justifyContent: 'space-between'
    },
    logoTrip: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logoDevPleno: {
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'center',
        paddingBottom: 32
    },
    botaoComecar: {
        backgroundColor: '#FFF',
        flex: 1,
        flexDirection: 'row',
        paddingBottom: 16,
        paddingTop: 16,
    },
    btnComecar: {
        textAlign: 'center',
        fontSize: 18
    },
    botaoEmptyBackground: {
        backgroundColor: '#FFF',
        paddingBottom: 16,
        paddingTop: 16,
        alignItems: 'center'
    },
    btnEmpty: {
        textAlign: 'center',
        fontSize: 18,
        width: 220
    },
    pin: {
        marginBottom: 16,
        marginTop: 16
    },
    arrowRight: {
        marginBottom: 16,
        marginTop: 16
    }
})