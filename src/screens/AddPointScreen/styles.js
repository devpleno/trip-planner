import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    campo: {
        backgroundColor: '#E5E5E5',
        padding: 20,
        marginBottom: 16
    },
    btnSalvar: {
        backgroundColor: 'darkgreen',
        padding: 20,
        marginBottom: 16
    }
}) 