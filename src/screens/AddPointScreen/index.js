import React, { Component } from 'react'
import { View, Text, Image, TextInput, TouchableOpacity, Dimensions, AsyncStorage } from 'react-native'

import MapView, { Marker } from 'react-native-maps'

import assets from './assets'
import styles from './styles'

class AddPointScreen extends Component {

    static navigationOptions = {
        header: null
    }

    state = {
        id: new Date().getTime(),
        position: {
            latitude: 37.78825,
            longitude: -122.4324
        },
        pointName: 'asd',
        description: 'dsa',
        price: 123
    }

    handleSave = async () => {
        const id = this.props.navigation.state.params.id

        const points = await AsyncStorage.getItem("trip-" + id)
        let colPoints = []

        if (points) {
            colPoints = JSON.parse(points)
        }

        colPoints.push(this.state)
        await AsyncStorage.setItem("trip-" + id, JSON.stringify(colPoints))

        let total = 0
        colPoints.forEach(p => {
            total += parseFloat(p.price)
        })

        // Pega a trip e atualiza o total
        const trips = await AsyncStorage.getItem("trips")
        let colTrips = []

        if (trips) {
            colTrips = JSON.parse(trips)
        }

        colTrips.forEach((t, i) => {
            if (t.id === id) {
                colTrips[i].price = total
                colTrips[i].latitude = colPoints[0].position.latitude
                colTrips[i].longitude = colPoints[0].position.longitude
            }
        })

        await AsyncStorage.setItem("trips", JSON.stringify(colTrips))

        this.props.navigation.state.params.refresh();
        this.props.navigation.goBack();
    }

    render() {

        return (
            <View style={{ flex: 1 }}>
                <View style={styles.imagem}>

                    <MapView
                        style={{ width: Dimensions.get('window').width, height: 200 }}

                        initialRegion={{
                            latitude: 37.78825,
                            longitude: -122.4324,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421
                        }}
                    >
                        <Marker draggable coordinate={this.state.position} onDragEnd={(ev) => this.setState({ position: ev.nativeEvent.coordinate })} />
                    </MapView>

                    <View style={styles.setaEsquerda}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Image source={assets.arrowLeft} />
                        </TouchableOpacity>
                    </View>

                    <TextInput style={styles.campo} placeholder="Nome do ponto" onChangeText={(ev) => this.setState({ pointName: ev })}></TextInput>
                    <TextInput style={styles.campo} placeholder="Descrição" onChangeText={(ev) => this.setState({ description: ev })}></TextInput>
                    <TextInput style={styles.campo} placeholder="Preço" onChangeText={(ev) => this.setState({ price: parseFloat(ev) })}></TextInput>

                    <TouchableOpacity style={styles.btnSalvar} onPress={this.handleSave}>
                        <Text>Salvar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

export default AddPointScreen