import { StyleSheet } from 'react-native'

export default styles = StyleSheet.create({
    setaEsquerda: {
        position: 'absolute',
        top: 16,
        left: 16,
        padding: 10
    },
    imagem: {
        height: 192,
        backgroundColor: 'grey'
    },
    titulo: {
        position: 'absolute', left: 16, bottom: 16
    },
    valor: {
        position: 'absolute',
        right: 16,
        bottom: 16,
        backgroundColor: '#24C6DC',
        padding: 4,
        color: 'white'
    },
    wrapperBackground: {
        flex: 1,
        flexDirection: 'row',
        paddingBottom: 16
    },
    wrapperTitle: {
        fontWeight: 'bold',
        fontSize: 18
    },
    wrapperPrice: {
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#24C6DC'
    },
    setaDireita: {
        position: 'absolute',
        top: 16,
        right: 16,
        padding: 10,
    },
    arrowUnion: {
        transform: [{ rotate: '45deg' }]
    },
    setaDireitaAbaixo: {
        position: 'absolute',
        top: 80,
        right: 16,
        padding: 10,
    }
}) 