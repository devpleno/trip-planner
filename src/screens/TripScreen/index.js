import React, { Component } from 'react'
import { View, Text, Image, FlatList, TouchableOpacity, AsyncStorage } from 'react-native'

import assets from './assets'
import styles from './styles'

class TripScreen extends Component {

    static navigationOptions = {
        header: null
    }

    state = {
        trip: {
            trip: '',
            price: 0
        },
        points: []
    }

    componentDidMount() {
        this.loadData()
    }

    loadData = async () => {
        const id = this.props.navigation.state.params.id

        const tripsAS = await AsyncStorage.getItem('trips')
        const trips = JSON.parse(tripsAS)

        trips.forEach(t => {
            if (t.id === id) {

                this.setState({
                    trip: t
                })
            }
        })

        const pointsAS = await AsyncStorage.getItem('trip-' + id)
        this.setState({
            points: JSON.parse(pointsAS)
        })
    }

    renderItem = (obj) => {
        return (
            <View style={styles.wrapperBackground}>
                <View style={{ flex: 1 }}>
                    <Text style={styles.wrapperTitle}>{obj.item.pointName}</Text>
                    <Text>{obj.item.description}</Text>
                </View>
                <View style={{ alignItems: 'center' }}>
                    <Text style={styles.wrapperPrice}>R$ {obj.item.price}</Text>
                </View>
            </View>
        )
    }

    removerTrip = async () => {
        let trips = await AsyncStorage.getItem("trips")
        let colTrips = []

        trips = JSON.parse(trips)

        trips.forEach(t => {
            if (this.state.trip.id !== t.id) {
                colTrips.push(t)
            }
        })

        await AsyncStorage.setItem("trips", JSON.stringify(colTrips))

        this.props.navigation.state.params.refresh();
        this.props.navigation.goBack();
    }

    render() {
        const { points, trip } = this.state

        return (
            <View style={{ flex: 1 }}>
                <View style={styles.imagem}>
                    <View style={styles.setaEsquerda}>
                        <TouchableOpacity onPress={() => { this.props.navigation.state.params.refresh(); this.props.navigation.goBack() }}>
                            <Image source={assets.arrowLeft} />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.setaDireita}>
                        <TouchableOpacity onPress={() => this.removerTrip()}>
                            <Image source={assets.arrowUnion} style={styles.arrowUnion} />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.setaDireitaAbaixo}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('AddPoint', { id: trip.id, refresh: this.loadData })}>
                            <Image source={assets.arrowMore} />
                        </TouchableOpacity>
                    </View>

                    <Text style={styles.titulo}>{trip.trip}</Text>
                    <Text style={styles.valor}>R$ {trip.price}</Text>
                </View>

                <FlatList style={{ flex: 1 }} data={points} renderItem={this.renderItem} contentContainerStyle={{ padding: 16 }} keyExtractor={i => i.id.toString()} />
            </View>
        )
    }
}

export default TripScreen